# trendkite

[![Travis-CI Build Status](https://travis-ci.org/JohnCoene/trendkite.svg?branch=master)](https://travis-ci.org/JohnCoene/trendkite) [![AppVeyor Build Status](https://ci.appveyor.com/api/projects/status/github/JohnCoene/trendkite?branch=master&svg=true)](https://ci.appveyor.com/project/JohnCoene/trendkite)

![trendkite](https://mms.businesswire.com/media/20180402005104/en/648868/4/3290292_TrendKite_Logo-1.jpg)

Integrates [Trendkite](https://www.trendkite.com/) API in R

## Install

This repository is private, you will need access and your credentials.

```R
devtools::install_bitbucket("JohnCoene/trendkite")
```

## Functions

* `tr_oauth` - create token
* `tr_searches` - list all searches
* `tr_create_search` - create a search
* `tr_mentions` - get mentions of search
* `tr_mentions_split` - split mentions call
* `tr_split` - split query
* `tr_date` - parse Trendkite date
* `tr_stats` - get aggregate data on search

Note: `token` returned by `tr_oauth` doesn't expire. **Call are limited to 5K results see `tr_mentions_split`**

## Details

`tr_split` and `tr_mentions_split` are convenience function that split the call in order to go around the rate limit of 5K results/call.

### Examples

```R
library(trendkite)

# authenticate
token <- tr_token("username", "password")

# list all searches
searches <- tr_searches(token)

# get mentions from random search from past 7 days
mentions <- tr_mentions(TK, sample(searches$id, 1), verbose = TRUE,
    start = Sys.time() - (7 * 24 * 60 * 60), end = Sys.time())
    
# split call
all_mentions <- tr_mentions_split(TK, sample(searches$id, 1), verbose = TRUE,
    start = Sys.time() - (7 * 24 * 60 * 60), end = Sys.time())
```
