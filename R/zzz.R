.onLoad <- function(libname = find.package("trendkite"), pkgname = "trendkite") {
  options(base_url = "https://api2.trendkite.com/api/")
}


.onUnload <- function(libpath = find.package("trendkite")) {
  options(base_url = NULL)
}

.onDetach <- function(libpath = find.package("trendkite")) {
  options(base_url = NULL)
}
