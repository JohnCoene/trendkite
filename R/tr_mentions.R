#' Get total mentions
#'
#' Get totalmentions for specific search.
#'
#' @param token as returned by \code{\link{tr_oauth}}.
#' @param s TrendKite search id (obtain from URL in Search Management page).
#' see details and examples.
#' @param start date of search query begin
#' @param end date of search query end
#' @param n number of results desired
#' @param verbose prints useful information to the console
#'
#' @examples
#' \dontrun{
#' TK <- tr_oauth("myUserName", "myPassword")
#'
#' # get list of all searches (required for tr_mentions)
#' searches <- tr_searches(TK)
#'
#' # search random search id last 15 days
#' news1 <- tr_mentions(TK, sample(searches$id, 1),
#'   start = Sys.time(),
#'   end = Sys.time() - (15 * 24 * 60 * 60))
#'
#' # search 2000 results last 3 days
#' news2 <- tr_mentions(TK, searches$id[1], n = 2000,
#'   start = Sys.time(),
#'   end = Sys.time() - (3 * 24 * 60 * 60))
#' }
#'
#' @seealso \code{\link{tr_split}} to fetch more data and \code{\link{tr_mentions_split}} for convenience
#'
#' @export
tr_mentions <- function(token, s, start, end, n = 1000, verbose = FALSE){

  if(n < 1000){
    warning("minimum 1000 results")
    n <- 1000
  }

  if(!is.POS(start) || !is.POS(end))
    stop("start and end must be of class POSIXct or POSIXlt or POSIXt", call. = FALSE)

  # input tests
  if(missing(token) || missing(s)) stop("missing token or s", call. = FALSE)
  if(missing(start) || missing(end)) stop("missing time start and end", call. = FALSE)

  start <- time_conv(start)
  end <- time_conv(end)

  uri <- paste0(getOption("base_url"),
                "v2/totalmentions",
                "?s=", s,
                "&range-start=", start,
                "&range-end=", end)

  pages <- ceiling(n/1000)

  list <- api_call(uri = uri, pages = pages, token, verbose)

  results <- parse_list(list)

  return(results)
}
