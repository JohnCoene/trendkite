#' Create a range split
#'
#' Split time range
#'
#' @param  start,end start and end date of time range in \code{POSIX} format.
#' @param by how to split, passed to \code{\link{seq.POSIXt}}
#'
#' @details The Trendkite API currently limits the number of results to 10'000 per query.
#' This lets you split the query to get more results.
#'
#' @return returns \code{data.frame} of range with \code{from} and \code{end} dates.
#'
#' @examples
#' # trailing 7 day splitted
#' (split <- tr_split(Sys.time()-(7 * 60 * 60 * 24), Sys.time()))
#'
#' @importFrom magrittr %>%
#'
#' @export
tr_split <- function(start, end, by = "day"){

  if(!is.POS(start) || !is.POS(end))
    stop("start and end must be of class POSIXct or POSIXlt or POSIXt", call. = FALSE)

  seq <- seq.POSIXt(start, end, by = by)
  data.frame(start = seq) %>%
    dplyr::mutate(end = dplyr::lead(start)) %>%
    dplyr::filter(!is.na(end)) %>%
    as.data.frame()
}

#' Create a range split
#'
#' Split time range
#'
#' @inheritParams tr_mentions
#' @param  start,end start and end date of time range in \code{POSIX} format.
#' @param by how to split, passed to \code{\link{seq.POSIXt}}
#'
#' @details The Trendkite API currently limits the number of results to 10'000 per query.
#' This lets you split the query to get more results.
#'
#' @return returns \code{data.frame} of results as returned by \code{tr_mentions} with
#' additional columns indicating the date range the respective resutl corresponds to; \code{start},
#' \code{end}.
#'
#' @examples
#' \dontrun{
#' set.seed(19880525) # for reproducability (last example)
#' TK <- tr_oauth("myUserName", "myPassword") # get token
#'
#' searches <- tr_searches(TK) # get searches
#'
#' # call random search
#' # split calls by day
#' tr_split_call(TK,
#'   s = sample(searches$id, 1),
#'   start = Sys.time()-(7 * 60 * 60 * 24),
#'   Sys.time(),
#'   n = 5000)
#'
#' # the above is the equivalent to:
#' ## Don't do that
#' split <- tr_split(Sys.time()-(7 * 60 * 60 * 24), Sys.time())
#'
#' results <- data.frame()
#' for(i in 1:nrow(split)){
#'   call <- tr_mentions(TK,
#'     sample(searches$id, 1),
#'     start = split$start[i],
#'     end = split$end[i])
#'
#'   results <- rbind.data.frame(results, split)
#' }
#' }
#'
#' @importFrom magrittr %>%
#'
#' @export
tr_mentions_split <- function(token, s, start, end, n = 5000, by = "day", verbose = FALSE){

  if(!is.POS(start) || !is.POS(end))
    stop("start and end must be of class POSIXct or POSIXlt or POSIXt", call. = FALSE)

  split <- tr_split(start, end, by)

  results <- data.frame()

  for(i in 1:nrow(split)){
    start <- split[i, "start"]
    end <- split[i, "end"]

    if(isTRUE(verbose))
      message("Calling from ", start, " to ", end)

    call <- tr_mentions(token, s, start, end, n, verbose)
    call[, "start"] <- start
    call[, "end"] <- end

    results <- plyr::rbind.fill(results, call)
  }

  results
}

#' Format date
#'
#' @param date date column
#'
#' @examples
#' \dontrun{
#' TK <- tr_oauth("myUserName", "myPassword")
#'
#' # get list of all searches (required for tr_mentions)
#' searches <- tr_searches(TK)
#'
#' news <- tr_mentions(TK, sample(searches$id, 1),
#'   start = Sys.time(),
#'   end = Sys.time() - (15 * 24 * 60 * 60)) %>%
#'   mutate(date = tr_date(date))
#' }
#'
#' @export
tr_date <- function(date){
  as.Date(date, "%m/%d/%y")
}
