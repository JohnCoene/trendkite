# trendkite 0.2.0

* `tr_stats` function added; returns aggregate data on search instead of individual articles.
* date time parsing function fixed.

# trendkite 0.1.0

* Initial version
